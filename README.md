# Vaultwarden

`docker-compose` files and others for [vaultwarden](https://github.com/dani-garcia/vaultwarden):

*"Alternative implementation of the Bitwarden server API written in Rust and compatible with upstream Bitwarden clients, perfect for self-hosted deployment where running the official resource-heavy service might not be ideal."*

Includes systemd service definitions for automatic db backup. These services presume the following:

- Vaultwarden's /data folder is located at /opt/vaultwarden
- A directory to store backups is available at /opt/backup
- `sqlite3` is installed on the system